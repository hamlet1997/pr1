<?php


class Controler
{
    public function __construct()
    {
        if ($_SERVER['REQUEST_METHOD']='POST') {
            $this->db=new mysqli('localhost', 'root', '', 'php_project');
            session_start();

            if (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0) {
                $this->dumbphoto();
            }
            if (isset($_POST['action'])) {
                if ($_POST['action']=='ajax1') {
                    $this->chekData();
                } elseif ($_POST['action']=='ajax3') {
                    $this->Ubdate();
                }elseif ($_POST['action']=='ajax4') {
                    $this->changesomething();
                }elseif ($_POST['action']=='ajax5') {
                    $this->showallphotos();
                }elseif ($_POST['action']=='ajax7') {
                    $this->removePhotos();
                }elseif ($_POST['action']=='ajax6') {
                    $this->searchusers();
                }elseif ($_POST['action']=='ajax8') {
                    $this->addfrineds();
                }
              
            }
            if (isset($_POST['btn'])) {
                $this->chekEmail();
            }
        }
    }

    
    public function chekData()
    {
        $name=$_POST['name'];
        $surname=$_POST['surname'];
        $age=$_POST['age'];
        $email=$_POST['email'];
        $password=$_POST['password'];
        $confirmpassword=$_POST['confirmpassword'];
        $gender=$_POST['gender'];
          
           
        $errors=[];
           
        if (empty($name)) {
            $errors['name_error']='name error' ;
        }
        if (empty($surname)) {
            $errors['surname_error']='surname error' ;
        }
        if (empty($age)) {
            $errors['age_error']='age is empty' ;
        } elseif (!filter_var($age, FILTER_VALIDATE_INT)) {
            $errors['age_error']='age error' ;
        }
        if (empty($email)) {
            $errors['email_error']='email is empty' ;
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors['email_error']='email error' ;
        }
        if (empty($password)) {
            $errors['error_password']='password error' ;
        } elseif (strlen(trim($password)) < 6) {
            $errors['error_password']='you need 6 and more simbols' ;
        }
        if (empty($confirmpassword)) {
            $errors['error_confirmpassword']='confirmpassword error' ;
        } elseif ($password !== $confirmpassword) {
            $errors['error_confirmpassword']='Password mismatch' ;
        }
            
        if (count($errors)>0) {
            print json_encode($errors);
        } else {
            if ($gender=="MA") {
                $photo ="image/txa.jpg";
            } else {
                $photo ="image/axjik.jpg";
            }
            $password = password_hash($password, PASSWORD_DEFAULT);
            $this->db->query("INSERT INTO users(`name`,`surname`,`age`,`email`,`password`,`gender`,image)
                VALUES('$name','$surname','$age','$email','$password','$gender','$photo')");
            print 1;
           
        }
    }
    public function chekEmail()
    {
        $login=$_POST['LogIn'];
        $Password=$_POST['Passowrd'];
        $errors=[];

        if (empty($login)) {
            $errors['login_error']='login error' ;
        }
        if (empty($Password)) {
            $errors['password_error']='password error' ;
        }
        $sql =  $this->db->query("SELECT * FROM users WHERE email='$login'")->fetch_all(true);
        print_r($sql);
        if (!$sql) {
            $errors['login_error']='login error' ;
        } elseif (!password_verify($Password, $sql[0]['password'])) {
            $errors['password_error']='password error' ;
        }
        print_r($errors);
        if (count($errors)>0) {
            $_SESSION['errors']=$errors;
            header('location:index.php');
        } else {
            $_SESSION['data']=$sql[0];
            header('location:profile.php');
        }
    }
    public function Ubdate()
    {
        $oldpas=$_POST['oldpas'];
        $newpas=$_POST['newpas'];
        $confnewpas=$_POST['confnewpas'];
        $id =  $_SESSION['data']["id"];
        $pass = $_SESSION['data']["password"];
        $errors=[];
        // print_r($id);
        if (empty($oldpas)) {
            $errors['oldpas_error']='oldpas error' ;
        }
        if (empty($newpas)) {
            $errors['newpas_error']='newpas error' ;
        }
        if (empty($confnewpas)) {
            $errors['confnewpas_error']='confnewpas error' ;
        }
        if ($oldpas==$newpas && !empty($oldpas) && !empty($newpas)) {
            $errors['error']='! wrong-> oldpas == newpas' ;
        }
        if (!password_verify($oldpas, $pass) && !empty($oldpas)) {
            $errors['oldpas_error']='password error' ;
        }
        if ($newpas!=$confnewpas) {
            $errors['error']='newpas is not equals confnewpas' ;
        }
        if (count($errors)>0) {
            $_SESSION['errors']=$errors;
            print json_encode($errors);
        } else {
            $newpas = password_hash($newpas, PASSWORD_DEFAULT);
            $this->db->query("UPDATE users SET password='$newpas' WHERE id= $id");
            print 1;
        }
    }
    public function changesomething()
    {
        $name=$_POST['oldpas'];
        $surname=$_POST['newpas'];
        $age=$_POST['confnewpas'];
        $id =  $_SESSION['data']["id"];

        $errors=[];
        // print_r($id);
        if (empty($name)) {
            $errors['name_error']='name error' ;
        }
        if (empty($surname)) {
            $errors['surname_error']='surname error' ;
        }
        if (empty($age)) {
            $errors['age_error']='age error' ;
        }
        if (count($errors)>0) {
            $_SESSION['errors']=$errors;
            print json_encode($errors);
        } else{
            $_SESSION['data']['name']=$name;
            $_SESSION['data']['surname']=$surname;
            $_SESSION['data']['age']=$age;
            $this->db->query("UPDATE users SET name='$name',surname='$surname',age='$age' WHERE id= $id");
            print 1;
            


        }
    }
    public function dumbphoto()
    {
        $filetmp = $_FILES["photo"]["tmp_name"];
        $filename = $_FILES["photo"]["name"];
        $id =  $_SESSION['data']["id"];

        print_r($_FILES["photo"]);
            if (!file_exists("image")) {
              mkdir('image');
            } 
            $photos="image/".time().$filename;
            move_uploaded_file($filetmp, $photos);
            $this->db->query("INSERT INTO photos (`image`,`users_id`)VALUES ('$photos',$id)");
            header('location:frends.php');
            print 1;
            
    }
    public function showallphotos()
    {
        $id =  $_SESSION['data']["id"];
            $img=$this->db->query("SELECT * from photos WHERE users_id=$id")->fetch_all(true);
            print json_encode($img);
            
    }
    public function searchusers()
    {
            $value =  $_POST['search'];
            $users=$this->db->query("SELECT * FROM users WHERE name LIKE '%$value%'")->fetch_all(true);
            print json_encode($users);
            
    }
    public function removePhotos()
    {
       
        $id_scr =  $_POST['src'];
        $id =  $_POST['photoid'];
            $usersphoto=$this->db->query("DELETE  from photos WHERE photos.id=$id");
            print json_encode($usersphoto);
            unlink($id_scr);
             print $id_scr;
            
    }
    public function addfrineds()
    {
       
        $My_id =  $_SESSION['data']["id"];
        $to_id=$_POST['to_id'];

        if (isset($to_id)) {
            $addfriends=$this->db->query("SELECT * from request WHERE $to_id=`users_id`");

        }else{
            $addfriends=$this->db->query("INSERT INTO request (`to_id`,`from_id`) value('$to_id','$My_id')");
            print json_encode($addfriends);
           
            print $My_id;
            print $to_id;
        }
             
            
    }
}
   
       
new Controler();

?>

